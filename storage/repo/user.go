package repo

import (
	pb "user_service/genproto/user"
)

// UserStorageI ...
type UserStorageI interface {
	CreateUser(*pb.UserReq) (*pb.UserResp, error)
	Getuser(*pb.Id) (*pb.UserResp,error)
}
