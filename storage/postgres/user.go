package postgres

import (
	"fmt"
	pb "user_service/genproto/user"

	"github.com/jmoiron/sqlx"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewUserRepo(db *sqlx.DB) *userRepo {
	return &userRepo{db: db}
}

// Create User ...
func (r *userRepo) CreateUser(user *pb.UserReq) (*pb.UserResp, error) {
	userResp := pb.UserResp{}
	err := r.db.QueryRow(`insert into users (username,
		email,
		rating) values($1, $2, $3) returning id,
		username,
		email,
		rating,
		created_at`, user.Username,
		user.Email,
		user.Rating).Scan(&userResp.Id,
		&userResp.Username,
		&userResp.Email,
		&userResp.Rating,
		&userResp.CreatedAt)
	if err != nil {
		return &pb.UserResp{}, err
	}
	fmt.Println("successfully")

	var favorites []*pb.Favorite
	for _, favv := range user.Favorites {
		favvResp := pb.Favorite{}
		fmt.Println("ok")
		err := r.db.QueryRow(`insert into favorites 
		(userid,
		kursid) 
		values ($1,$2) 
		returning userid,kursid`,
			userResp.Id,
			favv.Kursid).Scan(
			&favvResp.Userid,
			&favvResp.Kursid)
		if err != nil {
			return &pb.UserResp{}, err
		}

		favorites = append(favorites, &favvResp)
	}
	userResp.Favorites = favorites

	var userkurs []*pb.Userkurs
	for _, kurs := range user.Userkurss {
		kursResp := pb.Userkurs{}
		err := r.db.QueryRow(`insert into userkurs 
			(userid,
			kursid,
			state,
			present) 
			values ($1,$2,$3,$4) 
			returning userid,
			kursid,
			state,
			present`, userResp.Id, kurs.Kursid,
			kurs.State,
			kurs.Present).Scan(&kursResp.Userid,
			&kursResp.Kursid,
			&kursResp.State,
			&kursResp.Present)

		if err != nil {
			return &pb.UserResp{}, err
		}

		userkurs = append(userkurs, &kursResp)
	}
	userResp.Userkurss = userkurs

	return &userResp, nil
}

// Get User ...
func (r *userRepo) GetUser(user *pb.Id) (*pb.UserResp, error) {
	userResp := pb.UserResp{}
	err := r.db.QueryRow(`select id,
	username,
	email,
	rating,
	created_at from users where id=$1 AND deleted_at IS NULL`, user.Id)
	if err != nil {
		return &pb.UserResp{}, err
	}

	return &userResp, nil
}
