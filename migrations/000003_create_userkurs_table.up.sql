CREATE TABLE IF NOT EXISTS userkurs(
    userid INTEGER NOT NULL REFERENCES users(id),
    kursid serial not null,
    state varchar(50),
    present INTEGER
);