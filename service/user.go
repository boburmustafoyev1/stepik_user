package service

import (
	"context"

	"github.com/jmoiron/sqlx"
	pb "user_service/genproto/user"
	l "user_service/pkg/logger"
	"user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UserService ...
type UserService struct {
	storage storage.IStorage
	logger  l.Logger
}

// NewUserService ...
func NewUserService(db *sqlx.DB, log l.Logger) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *UserService) CreateUser(ctx context.Context, req *pb.UserReq) (*pb.UserResp, error) {
    user, err := s.storage.User().CreateUser(req)
    if err != nil {
        s.logger.Error("error insert", l.Any("error insert user", err))
        return &pb.UserResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
    }
	return user, nil
}

func (s *UserService) GetUser(ctx context.Context, req *pb.Id) (*pb.UserResp, error) {
    user, err := s.storage.User().Getuser(req)
    if err != nil {
        s.logger.Error("error get", l.Any("error get user", err))
        return &pb.UserResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
    }
	return user, nil
}